pub struct Paddle {
    pub position: (u32, u32),
    pub size: (u8, u8),
}

impl Paddle {
    fn area(&self) -> ((u32, u32), (u8, u8)) {
        (self.position, self.size)
    }

    pub fn get_paddle(&self) -> ((u32, u32), (u8, u8)) {
        Self::area(&self)
    }

    //    pub fn move_paddle(&self) -> &str {}
}
