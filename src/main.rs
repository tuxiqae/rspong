mod paddle;

fn main() {
    let pad1 = paddle::Paddle {
        position: (2, 30),
        size: (5, 20),
    };
    let pad2 = paddle::Paddle {
        position: (200, 30),
        size: (pad1.size),
    };

    println!(
        "Player #1 = pos: {:?}, size: {:?}, Player #2 pos: {:?}, size: {:?}",
        pad1.get_paddle().0,
        pad1.get_paddle().1,
        pad2.get_paddle().0,
        pad2.get_paddle().1
    );
}
